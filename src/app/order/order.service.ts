import {Order} from '../order/order.model';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class OrderService {
  private userId: string;

  private clientOrderHistoryUrl = 'https://taxihub-backend.herokuapp.com/orders/history/client/';
  private corporationOrderHistoryUrl = 'https://taxihub-backend.herokuapp.com/orders/history/corporation/';
  private driverOrderHistoryUrl = 'https://taxihub-backend.herokuapp.com/orders/history/driver/';

  private makeOrderUrl = 'https://taxihub-backend.herokuapp.com/orders';
  private activeOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/open/client/';
  private checkActiveOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/hasopen/client/';

  private cancelOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/';
  private takeOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/';
  private openDriverOrdersUrl = 'https://taxihub-backend.herokuapp.com/orders/open';
  private takenDriverOrdersUrl = 'https://taxihub-backend.herokuapp.com/orders/taken/driver/';
  private closeOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/';
  private resignFromOrderUrl = 'https://taxihub-backend.herokuapp.com/orders/';

  constructor(private http: HttpClient) {
    this.userId = localStorage.getItem('id');
  }

  getClientsHistory() {
    return this.http.get<Order[]>(this.clientOrderHistoryUrl + this.userId);
  }

  getCorporationHistory() {
    return this.http.get<Order[]>(this.corporationOrderHistoryUrl + this.userId);
  }

  getDriverHistory() {
    return this.http.get<Order[]>(this.driverOrderHistoryUrl + this.userId);
  }

  makeOrder(order: Order) {
    return this.http.post(this.makeOrderUrl, order);
  }

  getActiveOrder() {
    return this.http.get<Order>(this.activeOrderUrl + this.userId);
  }

  checkActiveOrder() {
    return this.http.get<boolean>(this.checkActiveOrderUrl + this.userId);
  }

  takeOrder(id: number) {
    return this.http.post(this.takeOrderUrl + id.toString() + '/taken/' + this.userId, {});
  }

  cancelOrder(id: number) {
    return this.http.post(this.cancelOrderUrl + id.toString() + '/cancelled', id);
  }

  getOpenOrderForDriver() {
    return this.http.get<Order[]>(this.openDriverOrdersUrl);
  }

  getTakenOrders() {
    return this.http.get<Order[]>(this.takenDriverOrdersUrl + this.userId);
  }

  closeOrder(id: number) {
    return this.http.post(this.closeOrderUrl + id.toString() + '/closed', id);
  }

  resignFromOrder(id: number) {
    return this.http.put(this.resignFromOrderUrl + id.toString() + '/resign', id);
  }
}
