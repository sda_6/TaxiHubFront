export class Order {
  public id: number;
  public driverId: number;
  public clientId: number;
  public fromLatitude: number;
  public fromLongitude: number;
  public fromAddress: string;
  public toLatitude: number;
  public toLongitude: number;
  public toAddress: string;
  public openTime: string;
  public startTime: string;
  public endTime: string;
  public status: string;

  constructor(clientId: number,
              fromLatitude: number,
              fromLongitude: number,
              fromAddress: string,
              toLatitude: number,
              toLongitude: number,
              toAddress: string) {
    this.clientId = clientId;
    this.fromLatitude = fromLatitude;
    this.fromLongitude = fromLongitude;
    this.fromAddress = fromAddress;
    this.toLatitude = toLatitude;
    this.toLongitude = toLongitude;
    this.toAddress = toAddress;
  }

}
