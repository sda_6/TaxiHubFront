import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {AppRoutingModule} from '../app-routing.module';
import {LoginService} from '../login/login.service';
import {AuthGuardDriver} from '../auth/auth-guard-driver.service';
import {AuthGuardClient} from '../auth/auth-guard-client.service';
import {AuthGuardCorporation} from '../auth/auth-guard-corporation.service';
import {NotificationService} from '../notification/notification.service';
import {AuthService} from '../auth/auth.service';
import {DriverService} from '../driver/driver.service';
import {CorporationService} from '../corporation/corporation.service';
import {ClientService} from '../client/client.service';
import {MapService} from '../map/map.service';
import {TokenInterceptor} from '../login/token.interceptor';
import {HeaderComponent} from './header/header.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CoreRoutingModule} from './core-routing.module';

@NgModule({
  declarations: [
    HeaderComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    CoreRoutingModule
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent
  ],
  providers: [
    LoginService,
    AuthGuardDriver,
    AuthGuardClient,
    AuthGuardCorporation,
    NotificationService,
    AuthService,
    DriverService,
    CorporationService,
    ClientService,
    MapService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
}
