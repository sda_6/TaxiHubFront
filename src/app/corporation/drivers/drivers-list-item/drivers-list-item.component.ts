import {Component, Input, OnInit} from '@angular/core';
import {Driver} from '../../../driver/driver.model';
import {CorporationService} from '../../corporation.service';
import {NotificationService} from '../../../notification/notification.service';

@Component({
  selector: 'app-drivers-list-item',
  templateUrl: './drivers-list-item.component.html',
  styleUrls: ['./drivers-list-item.component.css']
})
export class DriversListItemComponent implements OnInit {
  @Input() driver: Driver;

  constructor(private corporationService: CorporationService, private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  onSuspend() {
    this.corporationService.suspendDriver(this.driver.id).subscribe(
      () => this.notificationService.success('You successfully suspended driver!'),
      (error) => this.notificationService.error(error)
    );
  }

  onEnable() {
    this.corporationService.enableDriver(this.driver.id).subscribe(
      () => this.notificationService.success('You successfully enabled driver!'),
      (error) => this.notificationService.error(error)
    );
  }
}
