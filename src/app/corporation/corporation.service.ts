import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class CorporationService {
  private suspendUrl = 'https://taxihub-backend.herokuapp.com/corporation/driver/';
  private enableUrl = 'https://taxihub-backend.herokuapp.com/corporation/driver/';

  constructor(private http: HttpClient) {
  }

  register(corporation) {
    return this.http.post('https://taxihub-backend.herokuapp.com/corporation', corporation);
  }

  registerDriver(driver) {
    return this.http.post('https://taxihub-backend.herokuapp.com/corporation/driver', driver);
  }

  suspendDriver(id: number) {
    return this.http.put(this.suspendUrl + id.toString() + '/suspend', id);
  }

  enableDriver(id: number) {
    return this.http.put(this.enableUrl + id.toString() + '/enable', id);
  }
}
