import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {DriverComponent} from './driver/driver.component';
import {ClientComponent} from './client/client.component';
import {CorporationComponent} from './corporation/corporation.component';
import {DriversComponent} from './corporation/drivers/drivers.component';
import {ShowOrderComponent} from './driver/show-order/show-order.component';
import {MakeOrderComponent} from './client/make-order/make-order.component';
import {DriversListItemComponent} from './corporation/drivers/drivers-list-item/drivers-list-item.component';
import {OrderItemComponent} from './driver/show-order/order-item/order-item.component';
import {AgmCoreModule} from '@agm/core';
import {AgmDirectionModule} from 'agm-direction';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ActiveOrderComponent} from './client/active-order/active-order.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterModule} from 'angular5-toaster/dist';
import {HttpClientModule} from '@angular/common/http';
import {OrderHistoryComponent} from './order/order-history/order-history.component';
import {ConfirmComponent} from './confirm/confirm.component';
import {environment} from '../environments/environment';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {ProfileComponent} from './profile/profile.component';
import {RegistrationComponent} from './registration/registration.component';
import {RegistrationClientComponent} from './registration/registration-client/registration-client.component';
import {RegistrationCompanyComponent} from './registration/registration-company/registration-company.component';
import {OrderHistoryItemComponent} from './order/order-history/order-history-item/order-history-item.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {RegistrationDriverComponent} from './corporation/registration-driver/registration-driver.component';
import {CarEditComponent} from './car/car-edit/car-edit.component';
import {CoreModule} from './core/core.module';
import {OrdersTakenComponent} from './driver/orders-taken/orders-taken.component';
import {OrdersTakenItemComponent} from './driver/orders-taken/orders-taken-item/orders-taken-item.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DriverComponent,
    ClientComponent,
    CorporationComponent,
    DriversComponent,
    ShowOrderComponent,
    MakeOrderComponent,
    DriversListItemComponent,
    OrderHistoryComponent,
    OrderHistoryItemComponent,
    OrderItemComponent,
    ActiveOrderComponent,
    ConfirmComponent,
    RegistrationClientComponent,
    RegistrationCompanyComponent,
    ProfileComponent,
    RegistrationComponent,
    RegistrationDriverComponent,
    CarEditComponent,
    OrdersTakenComponent,
    OrdersTakenItemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToasterModule,
    AngularFireModule.initializeApp(environment.firebase, 'TaxiHub'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAUgiu4t-XEJPDsgrExygjaXC155TwjQaE',
      libraries: ['places']
    }),
    AgmDirectionModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    CoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
