import {Injectable} from '@angular/core';
import {Order} from '../order/order.model';

@Injectable()
export class MapService {

  constructor() {
  }

  getDirection(order: Order) {
    const origin = {lat: order.fromLatitude, lng: order.fromLongitude};
    const destination = {lat: order.toLatitude, lng: order.toLongitude};
    return {origin, destination};
  }
}
