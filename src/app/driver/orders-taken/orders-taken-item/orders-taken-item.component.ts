import {Component, Input, OnInit} from '@angular/core';
import {Order} from '../../../order/order.model';
import {OrderService} from '../../../order/order.service';
import {NotificationService} from '../../../notification/notification.service';

@Component({
  selector: 'app-orders-taken-item',
  templateUrl: './orders-taken-item.component.html',
  styleUrls: ['./orders-taken-item.component.css']
})
export class OrdersTakenItemComponent implements OnInit {
  @Input() order: Order;

  constructor(private orderService: OrderService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  onOrderClosed() {
    this.orderService.closeOrder(this.order.id).subscribe(
      () => this.notificationService.success('Order successfully closed!'),
      (error) => this.notificationService.error(error)
    );
  }

  onOrderResign() {
    this.orderService.resignFromOrder(this.order.id).subscribe(
      () => this.notificationService.success('You succesfully resign from order!'),
      (error) => this.notificationService.error(error)
    );
  }
}
