import {Component, OnInit} from '@angular/core';
import {Order} from '../../order/order.model';
import {OrderService} from '../../order/order.service';

@Component({
  selector: 'app-orders-taken',
  templateUrl: './orders-taken.component.html',
  styleUrls: ['./orders-taken.component.css'],
  providers: [OrderService]
})
export class OrdersTakenComponent implements OnInit {
  orders: Order[];
  p = 1;

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.orderService.getTakenOrders().subscribe((data: Order[]) => this.orders = data);
  }

  pageChanged(event) {
    this.p = event;
  }
}
