import {Component, Input, OnInit} from '@angular/core';
import {Order} from '../../../order/order.model';
import {OrderService} from '../../../order/order.service';
import {NotificationService} from '../../../notification/notification.service';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponent implements OnInit {
  @Input() order: Order;

  constructor(private orderService: OrderService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  onOrderTaken() {
    this.orderService.takeOrder(this.order.id).subscribe(
      () => this.notificationService.success('Order successfully taken!'),
      (error) => this.notificationService.error(error)
    );
  }
}
